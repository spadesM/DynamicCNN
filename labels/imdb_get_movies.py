import pandas as pd
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time

url = "https://www.imdb.com/search/title/"

driver = webdriver.Chrome(executable_path=r"C:\Users\mcaka\PycharmProjects\KDSRecommandation\chromedriver.exe")

# genres-26 genres id
driver.get(
    "https://www.imdb.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.imdb.com%2Fregistration%2Fap-signin-handler%2Fimdb_us&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=imdb_us&openid.mode=checkid_setup&siteState=eyJvcGVuaWQuYXNzb2NfaGFuZGxlIjoiaW1kYl91cyIsInJlZGlyZWN0VG8iOiJodHRwczovL3d3dy5pbWRiLmNvbS8_cmVmXz1sb2dpbiJ9&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&tag=imdbtag_reg-20")
driver.find_element_by_id('ap_email').send_keys("92king409@gmail.com")
driver.find_element_by_id('ap_password').send_keys("9Z7LYUw!^-/auHY")
driver.find_element_by_id('signInSubmit').click()
time.sleep(10)
start_begining = False
if start_begining:
    df = pd.DataFrame(columns=["metacritic", "title", "imdb_id", "rating", "genre", "vote_count"])
    for i in range(1, 27):
        driver.get(url)
        driver.find_element_by_name("num_votes-min").send_keys("10000")
        driver.find_element_by_name("release_date-min").send_keys("2000")
        driver.find_element_by_id(f"genres-{i}").click()
        driver.find_element_by_id("title_type-1").click()
        Select(driver.find_element_by_name('user_rating-min')).select_by_index(61)
        driver.find_element_by_id("my_ratings|exclude").click()
        Select(driver.find_element_by_id('search-count')).select_by_value("250")
        searched_genre = driver.find_element_by_css_selector(f"label[for=genres-{i}").text
        driver.find_element_by_xpath("/html/body/div[4]/div/div[2]/div[3]/form/div/p[3]/button").click()
        time.sleep(5)
        for j in range(1, 251):
            try:
                # /html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[1]/div[3]/p[4]/span[2]
                # /html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/p[4]/span[2]
                get_metacritic = lambda a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/div/div[3]/span"
                get_title = lambda a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/h3/a"
                get_rating = lambda a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/div/div[1]/strong"
                get_genres = lambda a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/p[1]/span[5]"
                get_vote_count = lambda a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/p[4]/span[2]"
                try:
                    metacritic = driver.find_element_by_xpath(get_metacritic(j)).text
                    metacritic = int(metacritic)
                    if metacritic < 70:
                        continue
                except:
                    continue
                try:
                    elem = driver.find_element_by_xpath(get_title(j))
                    title = elem.text
                    imdb_id = elem.get_attribute('href')
                except:
                    continue
                try:
                    rating = driver.find_element_by_xpath(get_rating(j)).text
                except:
                    continue
                try:
                    genres = driver.find_element_by_xpath(get_genres(j)).text
                    if not searched_genre in genres:
                        continue
                except:
                    continue
                try:
                    vote_count = driver.find_element_by_xpath(get_vote_count(j)).get_attribute("data-value")
                    vote_count = int(vote_count)
                except:
                    continue
                df = df.append({"metacritic": metacritic, "title": title, "imdb_id": imdb_id, "rating": rating,
                                "genre": searched_genre, "vote_count": vote_count}, ignore_index=True)

            except:
                continue

    df.to_excel("movies.xlsx", index=False)
else:
    df = pd.read_excel("movies.xlsx", engine='openpyxl')
    urls = {"https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=action&my_ratings=exclude&count=250&start=251&ref_=adv_nxt":"Action",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=comedy&my_ratings=exclude&count=250&start=251&ref_=adv_nxt":"Comedy",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=crime&my_ratings=exclude&count=250&start=251&ref_=adv_nxt":"Crime",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=drama&my_ratings=exclude&count=250&start=251&ref_=adv_nxt":"Drama",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=drama&my_ratings=exclude&count=250&start=501&ref_=adv_nxt":"Drama",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=drama&my_ratings=exclude&count=250&start=751&ref_=adv_nxt": "Drama",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=drama&my_ratings=exclude&count=250&start=1001&ref_=adv_nxt": "Drama",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=drama&my_ratings=exclude&count=250&start=1251&ref_=adv_nxt": "Drama",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=romance&my_ratings=exclude&count=250&start=251&ref_=adv_nxt":"Romance",
            "https://www.imdb.com/search/title/?title_type=feature&release_date=2000-01-01,&user_rating=7.0,&num_votes=10000,&genres=thriller&my_ratings=exclude&count=250&start=251&ref_=adv_nxt":"Thriller"}
    for key in urls.keys():
        driver.get(str(key))
        for j in range(1, 251):
            try:
                get_metacritic = lambda \
                    a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/div/div[3]/span"
                get_title = lambda a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/h3/a"
                get_rating = lambda \
                    a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/div/div[1]/strong"
                get_genres = lambda \
                    a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/p[1]/span[5]"
                get_vote_count = lambda \
                    a: f"/html/body/div[4]/div/div[2]/div[3]/div[1]/div/div[3]/div/div[{a}]/div[3]/p[4]/span[2]"
                try:
                    metacritic = driver.find_element_by_xpath(get_metacritic(j)).text
                    metacritic = int(metacritic)
                    if metacritic < 70:
                        continue
                except:
                    continue
                try:
                    elem = driver.find_element_by_xpath(get_title(j))
                    title = elem.text
                    imdb_id = elem.get_attribute('href')
                except:
                    continue
                try:
                    rating = driver.find_element_by_xpath(get_rating(j)).text
                except:
                    continue
                try:
                    genres = driver.find_element_by_xpath(get_genres(j)).text
                    if not urls[key] in genres:
                        continue
                except:
                    continue
                try:
                    vote_count = driver.find_element_by_xpath(get_vote_count(j)).get_attribute("data-value")
                    vote_count = int(vote_count)
                except:
                    continue
                df = df.append({"metacritic": metacritic, "title": title, "imdb_id": imdb_id, "rating": rating,
                                "genre": urls[key], "vote_count": vote_count}, ignore_index=True)

            except:
                continue
    df.to_excel("movies2.xlsx", index=False)

