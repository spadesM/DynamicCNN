# Dynamic Hybrid CNN yapılacaklar ve sorunlar

**Model geliştirilecek yerler
* SpadesLayer'da toplama bölmek daha mantıklı olacak
* elenmeden sonra 1'den fazla olursa senaryosu için test edilecek
* 

**Model ne zaman durdurulacak?**
* Başlangıç olarak sabit bir epoch sayısı verildi. Ancak hızlı öğrenen daha avantajlı
* Early Stopping uygulanabilir ancak eğitim uzayacaktır. 

**Model başarım ölçümleri ne olacak?**
* Train data loss seçildi.
* Sonrasında valid loss ve accuracy de eklenecek

**Modül Optimizasyonu**
* Modüllerin filtre sayıları sabit seçildi.
* Modüllerin filtre sayıları optimize edilmeli
* Aktivasyon fonksiyonlarının optimize edilmesi

**Modelin geliştirilmesi**
* model optimezeri SGD seçildi. Sonrasında diğer optimzerlar denenecek
* Loss fonksiyonu 'mean_squared_error' seçildi. Sonrasında başka losslar denenecek.
* Flatten ve Dense'in optimize edilmesi. Belki sonrasında en iyi model seçildikten sonra dense optimize edilebilir. 

**Muhtemel Sorunlar**
* modeli tekrar tekrar ederken zaten öğrenmiş olabilir. Bu yüzden sonraki modüller avantajlı olabilir. Bunun için model parametreleri sıfırlanabilir.
* Over-fitting için Cross Validation denenebilir. Ya da val_loss'a göre bakılmalı!
* bazı loss değerleri nan geldi. Batch Normalization Unutuldu belki ondandır.
* geçmişteki modelden daha kötü ama mevcut adaylardan daha iyiyse ne yapılmalı?
* Farklı ağlarda batch size yeterli kalmayabilir mi?
* 1.denemede döngüde kalmış olabilir?

**Görsel iyileştirmeler**
* Custom Callback yazılarak çıktı daha düzgün görünülmeli ve eğitim esnasında daha düzgün ve ne aşamada olduğu gösterilecek

> Ek olarak da ek araştırmalar yapılıp, benzer çalışmalar bulunacak.
> Cross Validation eklemek mantıklı mı?
