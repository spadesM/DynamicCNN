from tensorflow.keras import Sequential
import tensorflow as tf
from tensorflow.keras.applications import VGG19, ResNet50

from tensorflow.keras.preprocessing.image import ImageDataGenerator

from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import ReduceLROnPlateau

from tensorflow.keras.layers import Flatten, Dense, BatchNormalization, Activation, Dropout


def preprocess_image_input(input_images):
    input_images = input_images.astype('float32')
    output_ims = tf.keras.applications.resnet50.preprocess_input(input_images)
    return output_ims


'Import to_categorical from the keras utils package to one hot encode the labels'
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.datasets import cifar10

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
train_generator = ImageDataGenerator(
    rotation_range=2,
    horizontal_flip=True,
    zoom_range=.1)

val_generator = ImageDataGenerator(
    rotation_range=2,
    horizontal_flip=True,
    zoom_range=.1)

test_generator = ImageDataGenerator(
    rotation_range=2,
    horizontal_flip=True,
    zoom_range=.1)

lrr = ReduceLROnPlateau(
    monitor='val_accuracy',  # Metric to be measured
    factor=.01,  # Factor by which learning rate will be reduced
    patience=3,  # No. of epochs after which if there is no improvement in the val_acc, the learning rate is reduced
    min_lr=1e-5)  # The minimum learning rate
base_model_1 = VGG19(include_top=False, weights='imagenet', input_shape=(32, 32, 3), classes=y_train.shape[1])

base_model_2 = ResNet50(include_top=False, weights='imagenet', input_shape=(32, 32, 3), classes=y_train.shape[1])

model_1 = Sequential()
model_1.add(base_model_1)  # Adds the base model (in this case vgg19 to model_1)
model_1.add(
    Flatten())  # Since the output before the flatten layer is a matrix we have to use this function to get a vector of the form nX1 to feed it into the fully connected layers

# Add the Dense layers along with activation and batch normalization
model_1.add(Dense(1024, activation=('relu'), input_dim=512))
model_1.add(Dense(512, activation=('relu')))
model_1.add(Dense(256, activation=('relu')))
# model_1.add(Dropout(.3))#Adding a dropout layer that will randomly drop 30% of the weights
model_1.add(Dense(128, activation=('relu')))
# model_1.add(Dropout(.2))
model_1.add(Dense(10, activation=('softmax')))  # This is the classification layer
# Check final model summary

batch_size = 256
epochs = 100

learn_rate = .001

sgd = SGD(lr=learn_rate, momentum=.9, nesterov=False)
adam = Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)

model_1.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])

model_1.fit_generator(train_generator.flow(x_train, y_train, batch_size=batch_size),
                      epochs=epochs,
                      steps_per_epoch=x_train.shape[0] // batch_size,
                      validation_data=val_generator.flow(x_test, y_test, batch_size=batch_size),
                      validation_steps=250,
                      callbacks=[lrr], verbose=1)
