              precision    recall  f1-score   support

    airplane      0.706     0.742     0.724      1000
  automobile      0.780     0.800     0.790      1000
        bird      0.582     0.586     0.584      1000
         cat      0.491     0.460     0.475      1000
        deer      0.639     0.598     0.618      1000
         dog      0.564     0.565     0.565      1000
        frog      0.724     0.755     0.739      1000
       horse      0.726     0.738     0.732      1000
        ship      0.801     0.803     0.802      1000
       truck      0.759     0.745     0.752      1000

    accuracy                          0.679     10000
   macro avg      0.677     0.679     0.678     10000
weighted avg      0.677     0.679     0.678     10000
