              precision    recall  f1-score   support

    airplane      0.722     0.718     0.720      1000
  automobile      0.793     0.765     0.779      1000
        bird      0.594     0.557     0.575      1000
         cat      0.496     0.452     0.473      1000
        deer      0.619     0.627     0.623      1000
         dog      0.581     0.597     0.589      1000
        frog      0.702     0.788     0.743      1000
       horse      0.726     0.714     0.720      1000
        ship      0.795     0.806     0.800      1000
       truck      0.724     0.746     0.735      1000

    accuracy                          0.677     10000
   macro avg      0.675     0.677     0.676     10000
weighted avg      0.675     0.677     0.676     10000
