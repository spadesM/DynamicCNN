from tensorflow.keras.layers import Conv2D, MaxPooling2D, concatenate, add, Activation
from tensorflow.keras.layers import Layer
from tensorflow.keras.applications import Xception
from tensorflow.keras.applications import ResNet101


class Xception_model(Layer):
    def __init__(self, **kwargs):
        super(Xception_model, self).__init__(**kwargs)
        self.xception = Xception(include_top=False, classes=100)

    def call(self, input_layer, **kwargs):
        return self.xception(input_layer)

    def get_config(self):
        config = super().get_config().copy()
        return config


class ResNet101_model(Layer):
    def __init__(self, **kwargs):
        super(ResNet101_model, self).__init__(**kwargs)
        self.resnet = ResNet101(include_top=False, classes=100)

    def call(self, input_layer, **kwargs):
        return self.resnet(input_layer)

    def get_config(self):
        config = super().get_config().copy()
        return config
