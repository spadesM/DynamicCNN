import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import itertools


def plot_confusion_matrix(cm,
                          target_names=None, path=None,
                          cmap=None):

    title = 'Confusion matrix'
    accuracy = np.trace(cm) / float(np.sum(cm))

    if cmap is None:
        cmap = plt.get_cmap('Blues')
    plt.figure(figsize=(cm.shape[0], cm.shape[0]))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is None:
        target_names = list(range(cm.shape[0]))
    tick_marks = np.arange(len(target_names))
    plt.xticks(tick_marks, target_names, rotation=45)
    plt.yticks(tick_marks, target_names)

    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, "{:,}\n({:0.3f})".format(cm[i, j], cm_normalized[i,j]),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f};'.format(accuracy))
    if path is not None:
        plt.savefig(path)


def get_inception_parameter_count(input_shape, f1, f2_in, f2_out, f3_in, f3_out, f4,):
    pass


def get_module_parameter_count():
    pass


def get_resnet_parameter_count():
    pass


def get_vgg_parameter_count():
    pass