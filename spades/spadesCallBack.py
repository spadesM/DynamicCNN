from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import Callback


class SpadesCallback(Callback):
    def __init__(self, min_threshold, max_threshold):
        super(SpadesCallback, self).__init__()
        self.min_threshold = min_threshold
        self.max_threshold = max_threshold

    def on_train_begin(self, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("Starting training; got log keys: {}".format(keys), flush=True)

    def on_train_end(self, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("Stop training; got log keys: {}".format(keys))

    def on_epoch_begin(self, epoch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("Start epoch {} of training; got log keys: {}".format(epoch, keys))

    def on_epoch_end(self, epoch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("End epoch {} of training; got log keys: {}".format(epoch, keys))

    def on_test_begin(self, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("Start testing; got log keys: {}".format(keys))

    def on_test_end(self, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("Stop testing; got log keys: {}".format(keys))

    def on_predict_begin(self, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("Start predicting; got log keys: {}".format(keys))

    def on_predict_end(self, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("Stop predicting; got log keys: {}".format(keys))

    def on_train_batch_begin(self, batch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("...Training: start of batch {}; got log keys: {}".format(batch, keys))

    def on_train_batch_end(self, batch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("...Training: end of batch {}; got log keys: {}".format(batch, keys))

    def on_test_batch_begin(self, batch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("...Evaluating: start of batch {}; got log keys: {}".format(batch, keys))

    def on_test_batch_end(self, batch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("...Evaluating: end of batch {}; got log keys: {}".format(batch, keys))

    def on_predict_batch_begin(self, batch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("...Predicting: start of batch {}; got log keys: {}".format(batch, keys))

    def on_predict_batch_end(self, batch, logs=None):
        if logs is not None:
            keys = list(logs.keys())
            print("...Predicting: end of batch {}; got log keys: {}".format(batch, keys))

    def set_model(self, model):
        new_model = self.model_input
        layer_names = [layer.name for layer in model.layers]
        max_threshold = self.max_threshold
        min_threshold = self.min_threshold
        for idx, layer in enumerate(model.layers):
            if idx == 0:
                continue
            if idx == len(model.layers) - 1:
                new_model = layer(new_model)
            elif 'spades_layer' in layer.name:
                module_candidates = []  # layer.inbound_nodes[0].input_tensors
                found_max = False
                for w in range(layer.w.shape[0]):
                    if (layer.w[w] > min_threshold) and not found_max:
                        module_candidates.append(w)
                    if layer.w[w] > max_threshold:
                        module_candidates = [w]
                        max_threshold = layer.w[w]
                        found_max = True
                if len(module_candidates) == 1:
                    new_model = model.layers[layer_names.index(
                        layer.inbound_nodes[0].input_tensors[module_candidates[0]].name.split('/')[0])](new_model)
                elif len(module_candidates) < 1:
                    return model
                else:
                    temp_layers = []
                    for candidate in module_candidates:
                        temp_layers.append(model.layers[layer_names.index(
                            layer.inbound_nodes[0].input_tensors[module_candidates[candidate]].name)](new_model))
                    new_model = self.spades_block(temp_layers)
            elif 'spades_layer' in layer.outbound_nodes[0].output_tensors.name:
                continue
            else:
                new_model = layer(new_model)
        self.model = Model(inputs=self.model_input, outputs=new_model)