import math
import os
import json
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping, CSVLogger
from tensorflow.keras.layers import GlobalAveragePooling2D, Input, Flatten, Dense, BatchNormalization, Dropout, Conv2D, \
    Activation
from tensorflow.keras.utils import plot_model
from tensorflow.keras.models import Model
from spades.spadesLayer import SelectionLayer
import spades.applications as applications
import tensorflow as tf
from datetime import datetime
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, classification_report
from spades.utils import plot_confusion_matrix
from spades.applications_models import ResNet101_model, Xception_model
import numpy as np
from tensorflow.keras.optimizers import Adam, SGD


class Spades:
    def __init__(self, input_shape, logs=None, labels=None):
        self.model = None
        self.model_candidates = []
        self.model_input = Input(input_shape)  # Input(x_train[0].shape)
        self.core_model = self.model_input
        self.compile_kwargs = None
        self.candidate_cnt = 1
        self.min_threshold = 0.2
        self.max_threshold = 0.8
        self.logs = logs
        self.logs['min_threshold'] = self.min_threshold
        self.logs['max_threshold'] = self.max_threshold
        self.labels = labels

    @staticmethod
    def spades_block(layers):
        layer_in = SelectionLayer(input_dim=len(layers))(layers)
        return layer_in

    @staticmethod
    def write_log(log, path):
        with open(path + "/logs.json", 'w') as f:
            json.dump(log, f, indent=4)

        log_simple = log.copy()
        for key in log.keys():
            if "history" in key:
                log_simple.pop(key, None)
        with open(path + "/logs_simple.json", 'w') as f:
            json.dump(log_simple, f, indent=4)

    @staticmethod
    def draw_confusion_matrix(y_true, y_pred, labels, path):
        with open(path + 'classification_report.txt', 'w') as f:
            f.write(classification_report([labels[i] for i in y_true.argmax(axis=1)],
                                          [labels[i] for i in y_pred.argmax(axis=1)], digits=3))
        matrix = confusion_matrix(y_true.argmax(axis=1), y_pred.argmax(axis=1))
        plot_confusion_matrix(matrix, labels, path=path + "confusion_matrix.png")

    def plot_model(self):
        plot_model(self.core_model, show_shapes=True)

    def add_layers(self, n_filters, add_max_pooling=False):
        layer_vgg = applications.VGGModule(n_filters, 2, add_max_pooling)(self.core_model)
        # layer_residual = applications.ResNetModule(n_filters, add_max_pooling)(self.core_model)
        layer_residual = applications.MultiResnetModule(n_filters, 4, add_max_pooling)(self.core_model)
        f4 = n_filters - int(n_filters / 4) - int(n_filters / 2) - int(n_filters / 8)
        # layer_inception = applications.InceptionModuleV1(int(n_filters / 4), 3 * int(n_filters / 8),
        #                                                 int(n_filters / 2), int(n_filters / 16),
        #                                                 int(n_filters / 8), f4, add_max_pooling
        #                                                 )(self.core_model)
        layer_inception = applications.MultiInceptionModuleV1(int(n_filters / 4), 3 * int(n_filters / 8),
                                                              int(n_filters / 2), int(n_filters / 16),
                                                              int(n_filters / 8), f4, 5, add_max_pooling
                                                              )(self.core_model)
        self.core_model = self.spades_block([layer_vgg, layer_residual, layer_inception])
        self.core_model = BatchNormalization()(self.core_model)
        self.candidate_cnt += 1

    def add_models(self):
        res_model = ResNet101_model()(self.core_model)
        xception_model = Xception_model()(self.core_model)
        self.core_model = self.spades_block([res_model, xception_model])
        self.candidate_cnt += 1

    def fit(self, patience=True, rebuild=False, **kwargs):
        path = "results/" + datetime.now().strftime("%Y%m%d%H%M%S")
        if not os.path.isdir("results"):
            os.mkdir("results")
        if not os.path.isdir(path):
            os.mkdir(path)
        tf.config.experimental_run_functions_eagerly(True)
        plot_model(self.core_model, to_file=path + "/0model.png", show_shapes=True)
        with open(path + '/0model_summary.txt', 'w') as f:
            self.core_model.summary(print_fn=lambda x: f.write(x + '\n'))
        try:
            cnt = 0
            hist = self.core_model.fit(**kwargs, callbacks=[CSVLogger(path + "/0log.csv"),
                                                            EarlyStopping(monitor='val_loss', patience=kwargs['epochs']),
                                                            ReduceLROnPlateau(monitor='val_loss',
                                                                              factor=0.2,
                                                                              patience=10,
                                                                              min_lr=1e-6,
                                                                              cooldown=1,
                                                                              verbose=1, mode='max')
                                                            ])
            self.check_has_indecision(self.core_model)
            x_test = None
            y_test = None
            if "validation_data" in kwargs.keys():
                '''
                for d in kwargs["validation_data"]:
                    (x1, y1) = d
                    if x_test is None:
                        x_test = x1
                        y_test = y1
                    else:
                        x_test = np.concatenate((x_test, x1.numpy()), axis=0)
                        y_test = np.concatenate((y_test, y1.numpy()), axis=0)
                '''
                x_test, y_test = kwargs["validation_data"]

                if self.labels is None:
                    self.labels = list(range(y_test.shape[1]))
                y_pred = self.core_model.predict(x_test)
                # y_test = tf.keras.utils.to_categorical(y_test, max(y_test) + 1)
                self.draw_confusion_matrix(y_test, y_pred, self.labels, path + "/0")
                # self.core_model.save(path + "/" + "0model.h5")

            self.logs['history_base'] = {}
            for key in hist.history.keys():
                self.logs['history_base'][key] = [float(np_float) for np_float in hist.history[key]]

            force_remove = False
            while True:
                cnt += 1
                self.core_model, log = self.configure_model(self.core_model, self.min_threshold, self.max_threshold,
                                                            force_remove, rebuild)
                if log == {}:
                    force_remove = True
                else:
                    force_remove = False or (not patience)
                self.logs['modified' + str(cnt)] = log
                if self.compile_kwargs is not None:
                    self.core_model.compile(**self.compile_kwargs, optimizer=Adam())
                else:
                    self.core_model.compile()
                plot_model(self.core_model, to_file=path + "/" + str(cnt) + "model.png", show_shapes=True)
                with open(path + '/' + str(cnt) + 'model_summary.txt', 'w') as f:
                    self.core_model.summary(print_fn=lambda x: f.write(x + '\n'))
                hist = self.core_model.fit(**kwargs, callbacks=[CSVLogger(path + "/" + str(cnt) + "log.csv"),
                                                                EarlyStopping(monitor='val_loss', patience=50),
                                                                ReduceLROnPlateau(monitor='val_loss',
                                                                                  factor=0.2,
                                                                                  patience=10,
                                                                                  min_lr=1e-7,
                                                                                  cooldown=1,
                                                                                  verbose=1, mode='max')
                                                                ])
                if "validation_data" in kwargs.keys() and x_test is not None and y_test is not None:
                    y_pred = self.core_model.predict(x_test)
                    self.draw_confusion_matrix(y_test, y_pred, self.labels, path + "/" + str(cnt))
                # self.core_model.save(path + "/" + str(cnt) + "model.h5")
                self.logs['history_modified' + str(cnt)] = {}
                for key in hist.history.keys():
                    self.logs['history_modified' + str(cnt)][key] = [float(np_float) for np_float in hist.history[key]]
                if not self.check_has_spades(self.core_model):
                    if rebuild:
                        break
                    rebuild = True
            self.draw_results(path)
            self.write_log(self.logs, path)
        except KeyboardInterrupt:
            print('\nWriting results')
            self.draw_results(path)
            self.write_log(self.logs, path)

    @staticmethod
    def check_has_spades(model):
        for layer in model.layers:
            if 'selection_layer' in layer.name:
                return True
        return False

    def check_has_indecision(self, model):
        min_coefficient = 1
        layer2decide = None
        for layer in model.layers:
            if 'selection_layer' in layer.name:
                coefficient_layers = tf.keras.backend.eval(layer.w)
                # candidate_coefficients = candidate_coefficients / sum(candidate_coefficients)
                sum_coefficient_layers = sum(coefficient_layers)
                # sum([math.exp(coefficient_layer) for coefficient_layer in coefficient_layers])
                # coefficient_layers = [math.exp(coefficient_layer) / sum_coefficient_layers for coefficient_layer
                #                       in coefficient_layers]
                coefficient_layers = [coefficient_layer / sum_coefficient_layers for coefficient_layer
                                      in coefficient_layers]
                if min(coefficient_layers) < self.min_threshold or max(coefficient_layers) > self.max_threshold:
                    return None
                if min(coefficient_layers) < min_coefficient:
                    layer2decide = layer.name
                    min_coefficient = min(coefficient_layers)
        return layer2decide

    def draw_results(self, path):
        loss_list = self.logs["history_base"]["loss"].copy()
        val_loss_list = self.logs["history_base"]["val_loss"].copy()
        acc_list = self.logs["history_base"]["accuracy"].copy()
        val_acc_list = self.logs["history_base"]["val_accuracy"].copy()
        highlight_list = [len(loss_list)]
        cnt = 1
        while True:
            if not "history_modified" + str(cnt) in self.logs.keys():
                break
            loss_list += self.logs["history_modified" + str(cnt)]["loss"]
            val_loss_list += self.logs["history_modified" + str(cnt)]["val_loss"]
            acc_list += self.logs["history_modified" + str(cnt)]["accuracy"]
            val_acc_list += self.logs["history_modified" + str(cnt)]["val_accuracy"]
            highlight_list.append(len(loss_list))
            cnt += 1
        fig, (ax1, ax2) = plt.subplots(2)
        fig.suptitle('Loss and Accuracy')
        ax1.set_ylabel('loss')
        ax2.set_xlabel('epoch')
        ax1.set_ylim([0, max(max(loss_list), max(val_loss_list))])
        ax2.set_ylabel('accuracy')
        ax2.set_ylim([0, 1])
        ax1.plot(loss_list, label="train")
        ax1.plot(val_loss_list, label="val")
        ax1.legend()
        for highlight_elem in highlight_list:
            ax1.axvspan(highlight_elem - 0.1 - 1, highlight_elem + 0.1 - 1, color='red', alpha=0.5)
            ax2.axvspan(highlight_elem - 0.1 - 1, highlight_elem + 0.1 - 1, color='red', alpha=0.5)
        ax2.plot(acc_list, label="train")
        ax2.plot(val_acc_list, label="val")
        ax2.legend()
        plt.savefig(path + "/results.png", dpi=200)

    def configure_model(self, model, min_threshold, max_threshold, force_remove, rebuild=False):
        new_model = self.model_input
        layer_names = [layer.name for layer in model.layers]
        log = {}
        if self.check_has_indecision(model) is not None and not force_remove:
            return model, log
        for idx, layer in enumerate(model.layers):
            if idx == 0:
                continue
            if idx == len(model.layers) - 1:
                new_model = layer(new_model)
            elif 'selection_layer' in layer.name:
                candidate_coefficients = tf.keras.backend.eval(layer.w)
                sum_candidate_coefficient = sum(candidate_coefficients)
                # sum([math.exp(candidate_coefficient) for candidate_coefficient in candidate_coefficients])
                # candidate_coefficients = [math.exp(coefficient_layer) / sum_candidate_coefficient for
                # coefficient_layer in candidate_coefficients]
                candidate_coefficients = candidate_coefficients / sum(candidate_coefficients)
                module_candidates_dict = {}
                for candidate_idx, candidate_coefficient in enumerate(candidate_coefficients):
                    if candidate_coefficient > min_threshold:
                        module_candidates_dict[candidate_idx] = candidate_coefficient
                if len(module_candidates_dict.keys()) < 1:
                    module_candidates = [max(range(len(candidate_coefficients)),
                                             key=candidate_coefficients.__getitem__)]
                elif module_candidates_dict[max(module_candidates_dict, key=module_candidates_dict.get)] > \
                        max_threshold:
                    module_candidates = [max(module_candidates_dict, key=module_candidates_dict.get)]
                elif len(list(candidate_coefficients)) == len(list(module_candidates_dict.keys())) and \
                        layer.name == self.check_has_indecision(model):
                    temp_idx = [min(module_candidates_dict, key=module_candidates_dict.get)]
                    module_candidates = list(set(list(module_candidates_dict.keys())) -
                                             set(temp_idx)) + list(set(temp_idx) -
                                                                   set(list(module_candidates_dict.keys())))
                else:
                    module_candidates = list(module_candidates_dict.keys())
                tmp_dict = {}
                tmp_selected_list = []
                for i in range(len(candidate_coefficients)):
                    tmp_dict[layer.inbound_nodes[0].inbound_layers[i].name] = float(candidate_coefficients[i])
                    if i in module_candidates:
                        tmp_selected_list.append(layer.inbound_nodes[0].inbound_layers[i].name)
                log[layer.name + '_coefficients'] = tmp_dict
                log[layer.name + '_selected'] = tmp_selected_list
                if len(module_candidates) == 1:
                    tmp = layer.inbound_nodes[0].inbound_layers[module_candidates[0]]
                    if 'vgg_module' in tmp.name and rebuild:
                        new_layer = applications.VGGModule(tmp.n_filters, tmp.n_conv, tmp.add_max_pooling)
                    elif 'inception_module_v1_naive' in tmp.name and rebuild:
                        new_layer = applications.InceptionModuleV1Naive(tmp.f1, tmp.f2, tmp.f3)
                    elif 'inception_module_v1' in tmp.name and rebuild:
                        new_layer = applications.InceptionModuleV1(tmp.f1, tmp.f2_in, tmp.f2_out, tmp.f3_in,
                                                                   tmp.f3_out, tmp.f4, tmp.add_max_pooling)
                    elif 'resnet_module' in tmp.name and rebuild:
                        new_layer = applications.ResNetModule(tmp.n_filters, tmp.add_max_pooling)
                    elif 'multi_inception_module_v1' in tmp.name and rebuild:
                        new_layer = applications.MultiInceptionModuleV1(tmp.f1, tmp.f2_in, tmp.f2_out, tmp.f3_in,
                                                                        tmp.f3_out, tmp.f4, tmp.no_inception,
                                                                        tmp.add_max_pooling)
                    elif 'multi_resnet_module' in tmp.name and rebuild:
                        new_layer = applications.MultiResnetModule(tmp.n_filters, tmp.no_resnet,
                                                                   tmp.add_max_pooling)
                    else:
                        new_layer = model.layers[layer_names.index(layer.inbound_nodes[0].
                                                                   inbound_layers[module_candidates[0]].name)]
                    new_model = new_layer(new_model)
                elif len(module_candidates) < 1:
                    return model, log
                else:
                    temp_layers = []
                    for candidate in module_candidates:
                        tmp = layer.inbound_nodes[0].inbound_layers[candidate]
                        if 'vgg_module' in tmp.name and rebuild:
                            new_layer = applications.VGGModule(tmp.n_filters, tmp.n_conv, tmp.add_max_pooling)
                        elif 'inception_module_v1_naive' in tmp.name and rebuild:
                            new_layer = applications.InceptionModuleV1Naive(tmp.f1, tmp.f2, tmp.f3)
                        elif 'inception_module_v1' in tmp.name and rebuild:
                            new_layer = applications.InceptionModuleV1(tmp.f1, tmp.f2_in, tmp.f2_out, tmp.f3_in,
                                                                       tmp.f3_out, tmp.f4, tmp.add_max_pooling)
                        elif 'resnet_module' in tmp.name and rebuild:
                            new_layer = applications.ResNetModule(tmp.n_filters, tmp.add_max_pooling)
                        elif 'multi_inception_module_v1' in tmp.name and rebuild:
                            new_layer = applications.MultiInceptionModuleV1(tmp.f1, tmp.f2_in, tmp.f2_out, tmp.f3_in,
                                                                            tmp.f3_out, tmp.f4, tmp.no_inception,
                                                                            tmp.add_max_pooling)
                        elif 'multi_resnet_module' in tmp.name and rebuild:
                            new_layer = applications.MultiResnetModule(tmp.n_filters, tmp.no_resnet,
                                                                       tmp.add_max_pooling)
                        else:
                            new_layer = model.layers[layer_names.index(layer.inbound_nodes[0].
                                                                       inbound_layers[candidate].name)]

                        temp_layers.append(new_layer(new_model))
                    new_model = self.spades_block(temp_layers)
            elif 'selection_layer' in layer.outbound_nodes[-1].output_tensors.name:
                continue
            else:
                new_model = layer(new_model)
        return Model(inputs=self.model_input, outputs=new_model), log

    def compile(self, **kwargs):
        self.compile_kwargs = kwargs
        self.core_model = Model(inputs=self.model_input, outputs=self.core_model)
        self.core_model.compile(**self.compile_kwargs, optimizer=Adam())

    def add(self, layer):
        self.core_model = layer(self.core_model)

    def Flatten(self, **kwargs):
        self.core_model = tf.keras.layers.GlobalAveragePooling2D()(self.core_model)
        self.core_model = Flatten(**kwargs)(self.core_model)

    def GlobalAveragePooling2D(self, **kwargs):
        self.core_model = GlobalAveragePooling2D(**kwargs)(self.core_model)

    def Dense(self, **kwargs):
        self.core_model = Dense(**kwargs)(self.core_model)

    def Dropout(self, **kwargs):
        self.core_model = Dropout(**kwargs)(self.core_model)

    def summary(self, **kwargs):
        self.core_model.summary(**kwargs)

    def evaluate(self, x_test, y_test):
        return self.core_model.evaluate(x_test, y_test)

    def init_model(self):
        # self.core_model = tf.keras.layers.Lambda(lambda image: tf.image.resize(image, (136, 136)))(self.core_model)
        self.core_model = Conv2D(32, (3, 3), strides=(2, 2))(self.core_model)
        self.core_model = BatchNormalization()(self.core_model)
        self.core_model = Activation('relu')(self.core_model)
        self.core_model = Conv2D(32, (3, 3), padding='valid')(self.core_model)
        self.core_model = BatchNormalization()(self.core_model)
        self.core_model = Activation('relu')(self.core_model)
        self.core_model = Conv2D(32, (1, 1), strides=(1, 1), padding='valid')(self.core_model)
        self.core_model = BatchNormalization()(self.core_model)
        self.core_model = Activation('relu')(self.core_model)
        # self.core_model = MaxPooling2D((3, 3), strides=(2, 2))(self.core_model)
