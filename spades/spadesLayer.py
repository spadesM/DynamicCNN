from tensorflow.keras import initializers
from tensorflow.keras.constraints import min_max_norm
from tensorflow.keras.layers import Layer, Multiply, Add
from tensorflow import math
import random
import tensorflow as tf


class SelectionLayer(Layer):
    def __init__(self, input_dim, **kwargs):
        super(SelectionLayer, self).__init__(**kwargs)
        '''
        custom_initializer = []
        for i in range(input_dim - 1):
            custom_initializer.append(log(1 / (1 + exp(-1 / (input_dim - i)))))
        self.w = tf.Variable(initial_value=np.array(custom_initializer, dtype="float32"),  trainable=True)
        '''
        self.input_dim = input_dim
        self.w = self.add_weight(
            shape=self.input_dim, initializer=initializers.Constant(1/input_dim), trainable=True,
            constraint=min_max_norm(min_value=0.0, max_value=1.0),
            name="my_layer_name_{}".format
            (random.randint(1, 99999999))
        )

    def call(self, input_layers, **kwargs):
        sum_multiplier_coefficient = self.w[0]
        for idx in range(1, self.w.shape[0]):
            sum_multiplier_coefficient = tf.math.add(sum_multiplier_coefficient, self.w[idx])
        return_value = tf.math.multiply(input_layers[0], tf.math.divide(self.w[0], sum_multiplier_coefficient))
        for idx, input_layer in enumerate(input_layers):
            if idx == 0:
                continue
            return_value = tf.add(return_value, tf.math.multiply(input_layer,
                                                                 tf.math.divide(self.w[idx],
                                                                                sum_multiplier_coefficient)))

        return return_value
        """

        sum_w = math.exp(self.w[0])
        for i in range(1, len(input_layers)):
            sum_w = math.add(sum_w,  math.exp(self.w[i]))

        return_value = math.multiply(input_layers[0], math.divide(math.exp(self.w[0]), sum_w))
        for idx, input_layer in enumerate(input_layers):
            if idx == 0:
                continue
            return_value += math.multiply(input_layer, math.divide(math.exp(self.w[idx]), sum_w))

        return return_value"""

    def get_config(self):
        config = super().get_config().copy()
        return config

