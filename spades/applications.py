from tensorflow.keras.layers import Conv2D, MaxPooling2D, concatenate, add, Activation, BatchNormalization
from tensorflow.keras.layers import Layer


class InceptionModuleV1Naive(Layer):
    def __init__(self, f1, f2, f3, **kwargs):
        self.f1 = f1
        self.f2 = f2
        self.f3 = f3
        super(InceptionModuleV1Naive, self).__init__(**kwargs)
        self.conv1 = Conv2D(f1, (1, 1), padding='same', activation='relu')
        self.conv3 = Conv2D(f2, (3, 3), padding='same', activation='relu')
        self.conv5 = Conv2D(f3, (5, 5), padding='same', activation='relu')

    def call(self, input_layer, **kwargs):
        conv1 = self.conv1(input_layer)
        # 3x3 conv
        conv3 = self.conv3(input_layer)
        # 5x5 conv
        conv5 = self.conv5(input_layer)
        # 3x3 max pooling
        pool = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(input_layer)
        # concatenate filters, assumes filters/channels last

        return concatenate([conv1, conv3, conv5, pool], axis=-1)

    def get_config(self):
        config = super().get_config().copy()
        return config


class InceptionModuleV1(Layer):
    def __init__(self, f1, f2_in, f2_out, f3_in, f3_out, f4, add_max_pooling, **kwargs):
        super(InceptionModuleV1, self).__init__(**kwargs)
        self.f1 = f1
        self.f2_in = f2_in
        self.f2_out = f2_out
        self.f3_in = f3_in
        self.f3_out = f3_out
        self.f4 = f4
        self.conv1 = Conv2D(f1, (1, 1), padding='same', activation='relu')
        self.conv3_in = Conv2D(f2_in, (1, 1), padding='same', activation='relu')
        self.conv3 = Conv2D(f2_out, (3, 3), padding='same', activation='relu')
        self.conv5_in = Conv2D(f3_in, (1, 1), padding='same', activation='relu')
        self.conv5 = Conv2D(f3_out, (5, 5), padding='same', activation='relu')
        self.conv_pool = Conv2D(f4, (1, 1), padding='same', activation='relu')

        self.add_max_pooling = add_max_pooling

    def call(self, input_layer, **kwargs):
        conv1 = self.conv1(input_layer)
        # 3x3 conv
        conv3_in = self.conv3_in(input_layer)
        conv3 = self.conv3(conv3_in)
        # 5x5 conv
        conv5_in = self.conv5_in(input_layer)
        conv5 = self.conv5(conv5_in)
        # 3x3 max pooling
        pool_in = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(input_layer)
        pool = self.conv_pool(pool_in)  # /3 not sure
        # concatenate filters, assumes filters/channels last
        pool = concatenate([conv1, conv3, conv5, pool], axis=-1)
        if self.add_max_pooling:
            pool = MaxPooling2D((2, 2), strides=(2, 2))(pool)
        return pool

    def get_config(self):
        config = super().get_config().copy()
        return config


class VGGModule(Layer):
    def __init__(self, n_filters, n_conv, add_max_pooling, **kwargs):
        super(VGGModule, self).__init__(**kwargs)
        self.n_conv = n_conv
        self.n_filters = n_filters
        self.convs = []
        self.add_max_pooling = add_max_pooling
        for _ in range(n_conv):
            self.convs.append(Conv2D(self.n_filters, (3, 3), padding='same', activation='relu'))

    def call(self, input_layer, **kwargs):
        if len(self.convs) == 1:
            return self.convs[0](input_layer)
        layer = self.convs[0](input_layer)
        for i in range(1, len(self.convs)):
            layer = self.convs[i](layer)
        # out_layer = Conv2D(self.n_filters, (3, 3), padding='same', activation='relu')(input_layer)
        # add max pooling layer
        if self.add_max_pooling:
            layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)
        return layer

    def get_config(self):
        config = super().get_config().copy()
        return config


class ResNetModule(Layer):
    def __init__(self, n_filters, add_max_pooling,  **kwargs):
        super(ResNetModule, self).__init__(**kwargs)
        self.n_filters = n_filters
        self.bn_a = BatchNormalization(epsilon=1.001e-5)
        self.bn_b = BatchNormalization(epsilon=1.001e-5)
        self.bn_c = BatchNormalization(epsilon=1.001e-5)
        self.conv_0 = Conv2D(self.n_filters, (1, 1), padding='same', activation='relu',
                             kernel_initializer='he_normal')
        self.conv_a = Conv2D(self.n_filters / 4, (3, 3), padding='same', activation='relu',
                             kernel_initializer='he_normal')
        self.conv_b = Conv2D(self.n_filters / 4, (3, 3), padding='same', activation='relu',
                             kernel_initializer='he_normal')
        self.conv_c = Conv2D(self.n_filters, (1, 1), padding='same', activation='relu',
                             kernel_initializer='he_normal')
        self.add_max_pooling = add_max_pooling

    def call(self, input_layer, **kwargs):
        if input_layer.shape[-1] != self.n_filters:
            input_layer = self.conv_0(input_layer)

        conv0 = self.conv_a(input_layer)
        conv0 = self.bn_a(conv0)
        conv0 = Activation('relu')(conv0)
        # conv1
        conv1 = self.conv_b(conv0)
        conv1 = self.bn_b(conv1)
        conv1 = Activation('relu')(conv1)
        # conv2
        conv2 = self.conv_c(conv1)
        conv2 = self.bn_c(conv2)
        # add filters, assumes filters/channels last
        layer_out = add([conv2, input_layer])
        # activation function
        layer_out = Activation('relu')(layer_out)
        if self.add_max_pooling:
            layer_out = MaxPooling2D((2, 2), strides=(2, 2))(layer_out)
        return layer_out

    def get_config(self):
        config = super().get_config().copy()
        return config


class MultiInceptionModuleV1(Layer):
    def __init__(self, f1, f2_in, f2_out, f3_in, f3_out, f4, no_inception, add_max_pooling, **kwargs):
        super(MultiInceptionModuleV1, self).__init__(**kwargs)
        self.f1 = f1
        self.f2_in = f2_in
        self.f2_out = f2_out
        self.f3_in = f3_in
        self.f3_out = f3_out
        self.f4 = f4
        self.add_max_pooling = add_max_pooling
        self.incs = []
        self.no_inception = no_inception
        for _ in range(no_inception):
            self.incs.append(InceptionModuleV1(f1, f2_in, f2_out, f3_in, f3_out, f4, False))

    def call(self, input_layer, **kwargs):
        if len(self.incs) == 1:
            return self.incs[0](input_layer)
        layer = self.incs[0](input_layer)
        for i in range(1, len(self.incs)):
            layer = self.incs[i](layer)
        if self.add_max_pooling:
            layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)
        return layer

    def get_config(self):
        config = super().get_config().copy()
        return config


class MultiResnetModule(Layer):
    def __init__(self, n_filters, no_resnet, add_max_pooling, **kwargs):
        super(MultiResnetModule, self).__init__(**kwargs)
        self.n_filters = n_filters
        self.add_max_pooling = add_max_pooling
        self.resnets = []
        self.no_resnet = no_resnet
        for _ in range(no_resnet):
            self.resnets.append(ResNetModule(n_filters, False))

    def call(self, input_layer, **kwargs):
        if len(self.resnets) == 1:
            return self.resnets[0](input_layer)
        layer = self.resnets[0](input_layer)
        for i in range(1, len(self.resnets)):
            layer = self.resnets[i](layer)
        if self.add_max_pooling:
            layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)
        return layer

    def get_config(self):
        config = super().get_config().copy()
        return config
